Source: lazyarray
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Michael Hanke <mih@debian.org>, Yaroslav Halchenko <yoh@debian.org>
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-numpy <!nocheck>,
               python3-pytest <!nocheck>,
               python3-scipy <!nocheck>
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/science-team/lazyarray
Vcs-Git: https://salsa.debian.org/science-team/lazyarray.git
Homepage: https://github.com/NeuralEnsemble/lazyarray
Rules-Requires-Root: no

Package: python3-lazyarray
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         python3-numpy,
         python3-scipy
Description: Python module providing a NumPy-compatible lazily-evaluated array
 The 'larray' class is a NumPy-compatible numerical array where operations on
 the array (potentially including array construction) are not performed
 immediately, but are delayed until evaluation is specifically requested.
 Evaluation of only parts of the array is also possible. Consequently,
 use of an 'larray' can potentially save considerable computation time
 and memory in cases where arrays are used conditionally, or only parts of an
 array are used (for example in distributed computation, in which each MPI node
 operates on a subset of the elements of the array).
